SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `bill`;
CREATE TABLE `bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户UID',
  `bill_type_id` int(11) NOT NULL DEFAULT '0' COMMENT '消费类型ID',
  `currency_id` int(11) NOT NULL DEFAULT '0' COMMENT '货币类型ID',
  `number` varchar(25) NOT NULL DEFAULT '' COMMENT '流水号（订单编号）',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '此消费类变动额',
  `amount_lock` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '此消费类冻结额',
  `amount_balance` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '此消费类变动后余额',
  `amount_lock_balance` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '此消费类变动后冻结余额',
  `add_time` int(9) NOT NULL COMMENT '添加时间',
  `op_person` varchar(50) NOT NULL COMMENT '操作人昵称',
  `op_reason` varchar(255) NOT NULL DEFAULT '' COMMENT '操作原因',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`uid`,`bill_type_id`,`currency_id`,`number`,`amount`,`amount_lock`,`amount_balance`,`amount_lock_balance`,`add_time`) USING BTREE,
  KEY `NUMBER` (`number`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='流水记录表';

DROP TABLE IF EXISTS `bill_math`;
CREATE TABLE `bill_math` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `bill_type_id` int(11) NOT NULL DEFAULT '0' COMMENT '消费算法类型',
  `currency_index` int(11) NOT NULL DEFAULT '0' COMMENT '货币索引，从0开始，传入的第一种货币，使用规则0，第二种货币使用规则1，以此类推',
  `math` char(5) NOT NULL COMMENT '0=无增减,1=增加,-1=减少',
  `remark` varchar(500) DEFAULT '' COMMENT '算法描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='算法公式表';
INSERT INTO `bill_math`(`id`, `bill_type_id`, `currency_index`, `math`, `remark`) VALUES (1, 1, 0, '1,0', '充值成功，增加可用余额');
INSERT INTO `bill_math`(`id`, `bill_type_id`, `currency_index`, `math`, `remark`) VALUES (2, 2, 0, '-1,0', '用户消费，扣除可用余额');
INSERT INTO `bill_math`(`id`, `bill_type_id`, `currency_index`, `math`, `remark`) VALUES (3, 3, 0, '-1,1', '用户申请提现，扣除可用余额，增加冻结余额');
INSERT INTO `bill_math`(`id`, `bill_type_id`, `currency_index`, `math`, `remark`) VALUES (4, 4, 0, '0,-1', '用户提现完成，扣除冻结余额');
INSERT INTO `bill_math`(`id`, `bill_type_id`, `currency_index`, `math`, `remark`) VALUES (5, 5, 0, '1,-1', '用户提现驳回，扣除冻结余额，增加可用余额');

DROP TABLE IF EXISTS `bill_sum`;
CREATE TABLE `bill_sum` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户UID',
  `bill_type_id` int(11) NOT NULL COMMENT '消费类型ID',
  `currency_id` int(11) NOT NULL COMMENT '货币类型ID',
  `sum` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '此类消费货币总额',
  `lock_sum` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '此类消费货币冻结总额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='算法统计表';

DROP TABLE IF EXISTS `bill_type`;
CREATE TABLE `bill_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '算法id',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '算法名称',
  `remark` varchar(500) DEFAULT '' COMMENT '算法类型描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='算法类型表';
INSERT INTO `bill_type`(`id`, `name`, `remark`) VALUES (1, '充值', '用户充值操作');
INSERT INTO `bill_type`(`id`, `name`, `remark`) VALUES (2, '消费', '用户消费操作');
INSERT INTO `bill_type`(`id`, `name`, `remark`) VALUES (3, '提现申请', '用户申请提现');
INSERT INTO `bill_type`(`id`, `name`, `remark`) VALUES (4, '提现完成', '用户提现完成');
INSERT INTO `bill_type`(`id`, `name`, `remark`) VALUES (5, '提现驳回', '用户提现驳回');

DROP TABLE IF EXISTS `currency`;
CREATE TABLE `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资金类型id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '资金类型名称 如 资金\\金币\\积分 等',
  `code` varchar(255) NOT NULL DEFAULT '' COMMENT '英文名称(对应CURRENCY_BALANCE列名)',
  `unit` varchar(20) DEFAULT NULL COMMENT '货币单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='货币种类表';
INSERT INTO `currency`(`id`, `name`, `code`, `unit`) VALUES (1, '人民币', 'RMB', '元');

DROP TABLE IF EXISTS `currency_balance`;
CREATE TABLE `currency_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '用户uid',
  `amount_lock` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '冻结数量',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '数量',
  `currency_id` int(11) NOT NULL DEFAULT '0' COMMENT '货币类型id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='货币余额表';


SET FOREIGN_KEY_CHECKS = 1;
