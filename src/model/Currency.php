<?php
// +----------------------------------------------------------------------
// | Description: 货币类型 - CURRENCY
// +----------------------------------------------------------------------
// | Author: 38923
// +----------------------------------------------------------------------
// | Update: Markus <i@yoyoyo.me>
// +----------------------------------------------------------------------

namespace codelord\finance\model;

use think\Model;

class Currency extends Model {
    /**
     * 获取货币ID
     * @param string $name
     * @return int|null
     */
    public function getCurrencyId(string $name) : ? int
    {
        return $this->where('code',$name)->value('id');
    }
}
